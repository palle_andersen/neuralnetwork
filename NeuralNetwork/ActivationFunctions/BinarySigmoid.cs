﻿using static System.Math;

namespace NeuralNetwork.ActivationFunctions
{
    public class BinarySigmoid : IActivationFunction
    {

        private double Steepness {get; }

        public BinarySigmoid(double steepness = 1d)
        {
            Steepness = steepness;
        }

        public double Activate(double x) => 1d / (1d + Exp(-Steepness * x));
        
        public double Derivative(double x) => Steepness * Activate(x)*(1d-Activate(x));
    }
}
