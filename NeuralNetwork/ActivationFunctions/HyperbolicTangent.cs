﻿using static System.Math;

namespace NeuralNetwork.ActivationFunctions
{
    public class HyperbolicTangent : IActivationFunction
    {
        public double Activate(double x) => Tanh(x);

        public double Derivative(double x) => 1d - Pow(Activate(x), 2d);
    }
}
