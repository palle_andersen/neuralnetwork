﻿using System;

namespace NeuralNetwork.ActivationFunctions
{
    public class Step : IActivationFunction
    {
        
        public static IActivationFunction CreateWithThreshold(double threshold)
        {
            return new Step(threshold);
        }

        public Step(double threshold)
        {
            Threshold = threshold;
        }

        private double Threshold { get; }

        public double Activate(double x)
        {
            if (x < -Threshold) return -1d;
            if (x > Threshold) return 1d;
            return 0d;
        }

        public double Derivative(double x)
        {
            throw new NotImplementedException();
        }
    }
}
