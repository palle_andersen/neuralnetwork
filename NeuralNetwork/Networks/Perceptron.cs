﻿using NeuralNetwork.ActivationFunctions;
using System.Diagnostics;

namespace NeuralNetwork.Networks
{
    class Perceptron
    {
        private double[,] Weights { get; set; }
        private int[] OutputVector { get; set; }
        private double LearningRate { get; set; }
        public IActivationFunction Activation { get; set; }


        private int InputNodes { get; }
        private int OutputNodes { get; }
        
        public Perceptron(int inputNodes, int outputNodes)
        {
            InputNodes = inputNodes;
            OutputNodes = outputNodes;
        }

        protected virtual void Initialize()
        {
            OutputVector = new int[OutputNodes];
            Weights = new double[InputNodes + 1, OutputNodes];
            LearningRate = .1d;
            Activation = Step.CreateWithThreshold(.5d);
        }

        public int[] Process(int[] inputVector)
        {
            Debug.Assert(inputVector.Length == InputNodes);

            for (var j = 0; j < OutputVector.Length; j++)
            {
                OutputVector[j] = CalculateNetOutput(inputVector, j);
            }
            return OutputVector;
        }

        private int CalculateNetOutput(int[] inputVector, int j)
        {
            var y_in = Weights[Weights.GetUpperBound(0), j];  //Add weight from bias
            for (var i = 0; i < inputVector.Length; i++)
            {
                y_in += Weights[i, j] * inputVector[i];
            }
            var y = (int)Activation.Activate(y_in);

            return y;
        }

        public int Train(int[,] inputVectors, int[,] targets)
        {
            Debug.Assert(inputVectors.GetUpperBound(1) + 1 == InputNodes);
            Debug.Assert(targets.GetUpperBound(1) + 1 == OutputNodes);
            Debug.Assert(inputVectors.GetUpperBound(0) == targets.GetUpperBound(0));

            var epochs = 0;
            Initialize();

            var errorsOccured = true;

            while (errorsOccured && epochs < 10000)
            {
                epochs++;
                errorsOccured = false;
                for (var t = 0; t <= inputVectors.GetUpperBound(0); t++)
                {
                    for (var j = 0; j < OutputVector.Length; j++)
                    {
                        // Todo: Convert inputVectors to jagged array and just parse the Tth array to CalculateNetOutput
                        int[] inputVector = new int[inputVectors.GetUpperBound(1)+1];
                        for (var i = 0; i <= inputVectors.GetUpperBound(1); i++)
                        {
                            inputVector[i]= inputVectors[t, i];
                        }

                        var y = CalculateNetOutput(inputVector, j);
                        var target = targets[t, j];
                        if (target != y)
                        {
                            errorsOccured = true;
                            AdjustWeights(j, inputVector, y, target);
                        }
                    }
                }
            }
            return epochs;
        }

        private void AdjustWeights(int j, int[] inputVector, int y, int target)
        {
            var correction = LearningRate * (target - y);
            for (var i = 0; i < Weights.GetUpperBound(0); i++)
            {
                Weights[i, j] += correction * inputVector[i];
            }
            Weights[Weights.GetUpperBound(0), j] += correction;
        }
    }
}
