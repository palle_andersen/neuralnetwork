﻿using NeuralNetwork.Networks;
using System;

namespace NeuralNetwork
{
    class PerceptronDecoratedWithEvents : Perceptron
    {
        public event EventHandler Initializing;
        public event EventHandler Initialized;

        public PerceptronDecoratedWithEvents(int inputNodes, int outputNodes) : base(inputNodes, outputNodes)
        {
        }

        protected override void Initialize()
        {
            Initializing?.Invoke(this, new EventArgs());
            base.Initialize();
            Initialized?.Invoke(this, new EventArgs());
        }
    }
}
