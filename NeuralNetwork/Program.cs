﻿using NeuralNetwork.Networks;
using System;

namespace NeuralNetwork
{
    class Program
    {
        static void Main()
        {            
            var inputVectors = new[,] { { 0, 0 }, { 0, 1 }, { 1, 0 }, { 1, 1 }, };
            int epochs; 


            var and = new Perceptron(2,1);
            epochs = and.Train(
                inputVectors,
                new[,] {{0},{0},{0},{1},}
            );
            Console.WriteLine("Trained 'AND' in {0} epochs", epochs);

            var nand = new Perceptron(2, 1);
            epochs = nand.Train(
                inputVectors,
                new[,] { { 1 }, { 1 }, { 1 }, { 0 }, }
            );
            Console.WriteLine("Trained 'NAND' in {0} epochs", epochs);

            var or = new Perceptron(2, 1);
            epochs = or.Train(
                inputVectors,
                new[,] { { 0 }, { 1 }, { 1 }, { 1 }, }
            );
            Console.WriteLine("Trained 'OR' in {0} epochs", epochs);


            var implication = new Perceptron(2, 1);
            epochs = implication.Train(
                inputVectors,
                new[,] { { 1 }, { 1 }, { 0 }, { 1 }, }
            );
            Console.WriteLine("Trained 'IMPLICATION' in {0} epochs", epochs);

            Console.WriteLine("AND");
            TestNetwork(and, inputVectors);

            Console.WriteLine("NAND");
            TestNetwork(nand, inputVectors);

            Console.WriteLine("OR");
            TestNetwork(or, inputVectors);


            Console.WriteLine(or.Process(new[] { 0, 0 })[0]);

            Console.WriteLine("IMPLICATION");
            TestNetwork(implication, inputVectors);
        }

        private static void TestNetwork(Perceptron network, int[,] inputVectors)
        {
            var inputVector = new int[inputVectors.GetUpperBound(1)+1];

            for(var i=0; i<=inputVectors.GetUpperBound(0); i++)
            {
                for(var j=0;j<inputVector.Length;j++)
                {
                    inputVector[j] = inputVectors[i, j];
                    Console.Write("{0} ", inputVector[j] );
                }
                Console.Write("= ");
                
                var result = network.Process(inputVector);
                foreach(var e in result)
                {
                    Console.Write("{0} ", e);
                }
                Console.WriteLine();
            }
        }
    }
}
